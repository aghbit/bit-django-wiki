# -*- coding: utf-8 -*-

from wiki.core.plugins import registry
from wiki.core.plugins.base import BasePlugin
from . import markdown_extensions


class MathJaxPlugin(BasePlugin):
    slug = 'mathjax'
    
    class RenderMedia:
        js = [
            'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML',
            'wiki/js/mathjax.js',
        ]

    markdown_extensions = [markdown_extensions.MathJaxExtension()]

    def __init__(self):
        print "Loaded MathJaxPlugin"
        pass
    
registry.register(MathJaxPlugin)

