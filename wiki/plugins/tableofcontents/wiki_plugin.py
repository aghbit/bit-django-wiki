# -*- coding: utf-8 -*-

from wiki.core.plugins import registry
from wiki.core.plugins.base import BasePlugin
from . import markdown_extensions


class TableOfContentsPlugin(BasePlugin):
    slug = 'tableofcontents'

    markdown_extensions = [markdown_extensions.WikiTocExtension()]

    def __init__(self):
        print "Loaded TableOfContentsPlugin"
        pass
    
registry.register(TableOfContentsPlugin)

