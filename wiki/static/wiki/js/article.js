$(document).ready(function() {

  $('.wiki-article .article-list ul, .wiki-article .toc ul').each(function() {
    $(this).addClass('nav');
    $(this).addClass('nav-list');
  });

    var $window = $(window)
    var $body   = $(document.body)

    var navHeight = $('.navbar').outerHeight(true) + 10

    $body.scrollspy({
      target: '.bs-sidebar',
      offset: navHeight
    })

    // Recalculate scrollspy position after rendering page
    $window.on('load', function () {
      $body.scrollspy('refresh')
    })

    // Recalculate scrollspy positions after rendering math
    MathJax.Hub.Queue(function () {
        $body.scrollspy('refresh')
    });

    // Copied from bootstrap site
    setTimeout(function () {
      var $sideBar = $('.bs-sidebar')

      $sideBar.affix({
        offset: {
          top: function () {
            var offsetTop      = $sideBar.offset().top
            var sideBarMargin  = parseInt($sideBar.children(0).css('margin-top'), 10)
            var navOuterHeight = $('.navbar').height()

            return (this.top = offsetTop - navOuterHeight - sideBarMargin)
          }
        , bottom: function () {
            return (this.bottom = $('#wiki-footer').outerHeight(true))
          }
        }
      })


    }, 100)


    var offset = navHeight - 5;

    // Fix navbar issue while scrolling
    $('.bs-sidenav li a').click(function(event) {
      event.preventDefault();
      $($(this).attr('href'))[0].scrollIntoView();
      scrollBy(0, -offset);
    });

});


