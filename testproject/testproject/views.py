from django.views.generic import TemplateView


class CustomRootView(TemplateView):
    template_name = 'wiki/custom_root.html'