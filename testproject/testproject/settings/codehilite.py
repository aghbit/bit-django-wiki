from testproject.settings import *
from testproject.settings.local import *

# Test codehilite with pygments

WIKI_MARKDOWN_KWARGS = {'extensions': ['sane_lists', 'headerid', 'extra', 'codehilite']}

CUSTOM_ROOT_VIEW = 'testproject.views.CustomRootView'